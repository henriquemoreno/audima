# Arquitetura

O objetivo da arquitetura é criar um "microserviço" para que possa ter um servidor só para o crawler e outro servidor de arquivos (tipo S3 da AWS ou Google Storage) para os arquivos estáticos.

# WhishList

## PHP

* Usar Injeção de dependência para deixar o código mais modularizável.
* Colocar um pipeline no projeto usando o próprio gitlab para fazer o CI (pelo menos do bootstrap do projeto e dos testes unitários) e futuramente fazer um CD usando o próprio pipeline.
* Criar um serviço que use o google headless para fazer "crawler" e retorne o HTML, pois assim não teriamos que ficar fazendo fake do Header de browser e ele já traz muita informação que foi capturada via AJAX.
* Colocar um MQ e fazer um sistema assíncrono, colocando batchs para consumir, assim poderíamos diminuir a frequência e os WAF não identificariam como um DDOS ou algo do gênero.
* Migrar para que seja utilizado um AWS Lambda ou Google Funcion, e , um API Gateway.
