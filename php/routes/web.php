<?php
use Illuminate\Support\Facades\Request;
use App\Url;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/url', function() {
    return \App\Http\Resources\UrlResource::collection(Url::all());
});
Route::post('/url/{url}', function($url) {
    return \App\Http\Resources\UrlResource::collection(Url::saveUrl($url));
});