<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UrlRestTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest1()
    {
        $response = $this->get('/url');
        print_r($response);
        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest2()
    {
        $response = $this->post('/url', [
            'url' => 'http://wwww.google.com'
        ]);

        $response->assertStatus(200);
    }
}
