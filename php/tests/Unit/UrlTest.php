<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UrlTest extends TestCase
{
    use \Illuminate\Foundation\Testing\DatabaseMigrations;
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRegex()
    {
        $resultados = \App\Url::findUrls("jalkfjlksdfjk <a href=\"http://www.google.com.br\">Hello world</a> <a href='https://www.altavista.com'>Altavista.com</a>");
        $this->assertTrue(count($resultados) > 0);
        $this->assertTrue($resultados[0] == 'http://www.google.com.br');
        $this->assertTrue($resultados[1] == 'https://www.altavista.com');
        $resultados = \App\Url::findUrls("jalkfjlksdfjk jfakljfasldjfka");
        $this->assertTrue(count($resultados) == 0);
    }

    public function testIfUrlExists()
    {
        $this->assertFalse(\App\Url::hasUrl('http://www.google.com.br'));
        $url = new \App\Url();
        $url->url = 'http://www.google.com.br';
        $url->save();
        $this->assertTrue(\App\Url::hasUrl('http://www.google.com.br'));
    }

    public function testSaveUrl()
    {
        $url = \App\Url::saveUrl('http://www.altavista.com');
        $this->assertTrue($url->visited == false);
        try {
            \App\Url::saveUrl('http://www.altavista.com');
        } catch (\Exception $exception) {
            $this->assertTrue(true);
        }
    }
}
