<?php

namespace Tests\Unit;

use Tests\TestCase;

class EmailTest extends TestCase
{
    use \Illuminate\Foundation\Testing\DatabaseMigrations;
    use \Illuminate\Foundation\Testing\RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRegex()
    {
        $resultados = \App\Email::findEmails('pedro paulo pedropaulo@gmail.com joao batata joaobatata@gmail.com');
        $this->assertTrue(count($resultados) > 0);
        $this->assertTrue($resultados[0] == 'pedropaulo@gmail.com');
        $this->assertTrue($resultados[1] == 'joaobatata@gmail.com');
        $resultados = \App\Email::findEmails('pedro paulo');
        $this->assertTrue(count($resultados) == 0);
    }

    public function testIfExistsExists()
    {
        $this->assertFalse(\App\Email::hasEmail('teste@teste.com.br'));
        $url = new \App\Email();
        $url->email = 'teste@teste.com.br';
        $url->save();
        $this->assertTrue(\App\Email::hasEmail('teste@teste.com.br'));
        \App\Email::where('email', 'teste@teste.com.br')->delete();
        $this->assertFalse(\App\Email::hasEmail('teste@teste.com.br'));
    }
}
