<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'emails';
    public static function findEmails($conteudo) {
        preg_match_all('/\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i', $conteudo, $resultados);
        return $resultados[0];
    }

    public static function hasEmail($email) {
        return Email::where('email', trim($email))->count() > 0;
    }
}
