<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'urls';
    protected $fillable = ['url', 'visited'];

    /**
     * @param string
     * @return Url
     */
    public static function saveUrl($stringUrl) {
        if ( !Url::hasUrl($stringUrl)) {
            $url = new Url([
                "url" => $stringUrl,
                'visited' => false
                ]);
            $url->save();
            return $url;
        }
        throw new \Exception("URL já existe!"); 
    }

    public static function findUrls($conteudo) {
        preg_match_all('/<a href=["\']?((?:.(?!["\']?\s+(?:\S+)=|[>"\']))+.)["\']?>/i', $conteudo, $resultados);
        return $resultados[1];
    }

    public static function hasUrl($url) {
        return Url::where('url', trim($url))->count() > 0;
    }
}
